
  const myButton = document.getElementById('my-button');
  const myDiv = document.getElementById('my-div');

  myButton.addEventListener('click', () => {
    fetch('/bin/hrem/serv')
      .then(response => response.text())
      .then(data => {
        myDiv.innerHTML = data;
      })
      .catch(error => {
        console.error('Request failed. Error message: ' + error.message);
      });
  });
