/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.coderwanda__002d__rem.components.structure.gridproxy;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class gridproxy__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_list = null;
Object _global_template = null;
Object _global_itemtemplate = null;
Object _global_teasertemplate = null;
Collection var_collectionvar0_list_coerced$ = null;
Object _dynamic_properties = bindings.get("properties");
_global_list = renderContext.call("use", com.adobe.cq.wcm.core.components.models.List.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_itemtemplate = renderContext.call("use", "item.html", obj());
_global_teasertemplate = renderContext.call("use", "teaser.html", obj());
{
    Object var_collectionvar0 = renderContext.getObjectModel().resolveProperty(_global_list, "listItems");
    {
        long var_size1 = ((var_collectionvar0_list_coerced$ == null ? (var_collectionvar0_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar0)) : var_collectionvar0_list_coerced$).size());
        {
            boolean var_notempty2 = (var_size1 > 0);
            if (var_notempty2) {
                {
                    long var_end5 = var_size1;
                    {
                        boolean var_validstartstepend6 = (((0 < var_size1) && true) && (var_end5 > 0));
                        if (var_validstartstepend6) {
                            out.write("<ul");
                            {
                                Object var_attrvalue7 = renderContext.getObjectModel().resolveProperty(_global_list, "id");
                                {
                                    Object var_attrcontent8 = renderContext.call("xss", var_attrvalue7, "attribute");
                                    {
                                        boolean var_shoulddisplayattr10 = (((null != var_attrcontent8) && (!"".equals(var_attrcontent8))) && ((!"".equals(var_attrvalue7)) && (!((Object)false).equals(var_attrvalue7))));
                                        if (var_shoulddisplayattr10) {
                                            out.write(" id");
                                            {
                                                boolean var_istrueattr9 = (var_attrvalue7.equals(true));
                                                if (!var_istrueattr9) {
                                                    out.write("=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                                                    out.write("\"");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            {
                                Object var_attrvalue11 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_list, "data"), "json");
                                {
                                    Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
                                    {
                                        boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
                                        if (var_shoulddisplayattr14) {
                                            out.write(" data-cmp-data-layer");
                                            {
                                                boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                                                if (!var_istrueattr13) {
                                                    out.write("=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                                                    out.write("\"");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            out.write(" class=\"cmp-list\">");
                            if (var_collectionvar0_list_coerced$ == null) {
                                var_collectionvar0_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar0);
                            }
                            long var_index15 = 0;
                            for (Object item : var_collectionvar0_list_coerced$) {
                                {
                                    boolean var_traversal17 = (((var_index15 >= 0) && (var_index15 <= var_end5)) && true);
                                    if (var_traversal17) {
                                        out.write("\r\n    ");
                                        {
                                            boolean var_testvariable18 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_list, "displayItemAsTeaser")));
                                            if (var_testvariable18) {
                                                out.write("<li class=\"cmp-list__item\"");
                                                {
                                                    Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(item, "data"), "json");
                                                    {
                                                        Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                                                            if (var_shoulddisplayattr22) {
                                                                out.write(" data-cmp-data-layer");
                                                                {
                                                                    boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                                                                    if (!var_istrueattr21) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">");
                                                {
                                                    Object var_templatevar23 = renderContext.getObjectModel().resolveProperty(_global_itemtemplate, "item");
                                                    {
                                                        Object var_templateoptions24_field$_list = _global_list;
                                                        {
                                                            Object var_templateoptions24_field$_item = item;
                                                            {
                                                                java.util.Map var_templateoptions24 = obj().with("list", var_templateoptions24_field$_list).with("item", var_templateoptions24_field$_item);
                                                                callUnit(out, renderContext, var_templatevar23, var_templateoptions24);
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("</li>");
                                            }
                                        }
                                        out.write("\r\n    ");
                                        {
                                            Object var_testvariable25 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_list, "displayItemAsTeaser")) ? renderContext.getObjectModel().resolveProperty(item, "teaserResource") : renderContext.getObjectModel().resolveProperty(_global_list, "displayItemAsTeaser")));
                                            if (renderContext.getObjectModel().toBoolean(var_testvariable25)) {
                                                out.write("<li class=\"cmp-list__item\"");
                                                {
                                                    Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(item, "data"), "json");
                                                    {
                                                        Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
                                                            if (var_shoulddisplayattr29) {
                                                                out.write(" data-cmp-data-layer");
                                                                {
                                                                    boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                                                                    if (!var_istrueattr28) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">");
                                                {
                                                    Object var_templatevar30 = renderContext.getObjectModel().resolveProperty(_global_teasertemplate, "teaser");
                                                    {
                                                        Object var_templateoptions31_field$_item = item;
                                                        {
                                                            java.util.Map var_templateoptions31 = obj().with("item", var_templateoptions31_field$_item);
                                                            callUnit(out, renderContext, var_templatevar30, var_templateoptions31);
                                                        }
                                                    }
                                                }
                                                out.write("</li>");
                                            }
                                        }
                                        out.write("\r\n        \r\n");
                                    }
                                }
                                var_index15++;
                            }
                            out.write("</ul>");
                        }
                    }
                }
            }
        }
    }
    var_collectionvar0_list_coerced$ = null;
}
out.write("\r\n\r\n");
{
    Object var_templatevar32 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions33_field$_isempty = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_list, "listItems"), "size"), 0));
        {
            String var_templateoptions33_field$_classappend = "cmp-list";
            {
                java.util.Map var_templateoptions33 = obj().with("isEmpty", var_templateoptions33_field$_isempty).with("classAppend", var_templateoptions33_field$_classappend);
                callUnit(out, renderContext, var_templatevar32, var_templateoptions33);
            }
        }
    }
}
out.write("\r\n\r\n<h1>");
{
    Object var_34 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_properties, "orderId"), "text");
    out.write(renderContext.getObjectModel().toString(var_34));
}
out.write("</h1>");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

