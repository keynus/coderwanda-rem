package it.coderwanda.rem.core.servlets;

import com.day.cq.commons.jcr.JcrConstants;
import com.google.gson.JsonObject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletPaths;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(service={Servlet.class})
@SlingServletPaths(value={"/bin/hrem/serv"})
@ServiceDescription("Sling Servlet by path")

public class MyFirstSlingServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse response) throws ServletException, IOException {
        final Resource resource = req.getResource();
        
        
        JsonObject jsonObject = new JsonObject();

        
        jsonObject.addProperty("Player", "Lillard");
        jsonObject.addProperty("Team", "Blazers");
        jsonObject.addProperty("Position", "PG");
        jsonObject.addProperty("Number", "0");

                                                                         // Set the content type to application/json
        response.setContentType("application/json");

                                                                            // Write the JsonObject to the response
        response.getWriter().write(jsonObject.toString());
    }
}
