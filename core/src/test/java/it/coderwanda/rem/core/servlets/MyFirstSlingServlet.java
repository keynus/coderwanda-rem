package it.coderwanda.rem.core.servlets;

import com.day.cq.commons.jcr.JcrConstants;
import com.google.gson.JsonObject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletPaths;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(service={Servlet.class})
@SlingServletPaths(value={"/bin/my-servlet"})
@ServiceDescription("this is my servlet")

public class MyFirstSlingServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse response) throws ServletException, IOException {
        final Resource resource = req.getResource();
        
        // Create a new instance of JsonObject
        JsonObject jsonObject = new JsonObject();

        // Add some properties to the JsonObject
        jsonObject.addProperty("name", "John Doe");
        jsonObject.addProperty("age", 30);
        jsonObject.addProperty("isMarried", true);

        // Set the content type to application/json
        response.setContentType("application/json");

        // Write the JsonObject to the response
        response.getWriter().write(jsonObject.toString());
    }
}
